Une agence de communication

De la création d'un logo à la gestion d'une campagne Adwords, on vous dit tout sur les agences de communication.

[Agence de communication](http://www.madycoders.com) : définition

Une agence de communication est une entreprise qui orchestre la communication interne ou / et externe pour le compte de ses clients.
La communication interne
Elle vise à fédérer le personnel aux objectifs de l’entreprise, exemple : le site internet, un intranet, des revues mensuelles, un journal d’entreprise,…
La communication externe

[madycoders](http://www.madycoders.com)

Elle vise à promouvoir l’image de l’entreprise auprès du public ciblé, exemple : le site internet, les réseaux sociaux, les cartes de visites, une plaquette commerciale,…
Les clients d’une agence de communication peuvent être des entreprises, des collectivités, des libéraux ou encore des associations. Les clients d’une agence de communication ne sont pas toujours des annonceurs. Les agences de communication globales gèrent l’intégralité du budget et des besoins en communication de leurs clients.
Une agence de communication est à l’écoute de ses clients, elle commence par intégrer une série de paramètres : le secteur d’activité, les objectifs, la concurrence, le budget, …
Une fois tous ces paramètres analysés, elle propose une stratégie de communication. Après quelques échanges et la validation du client, l’agence de communication met en place un plan de communication. Vient ensuite la diffusion du ou des messages et la mesure de la performance de l’action de communication. 
